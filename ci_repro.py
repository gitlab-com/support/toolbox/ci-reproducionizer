#!/usr/bin/env python3

"""
Quickly transform CI config files to make them "executable" outside of their normal context
"""


import os
import shutil
import argparse
import subprocess

CLEAN_SUFFIX = "-clean"
NORMALIZED_SUFFIX = "-normalized"


operations = [
# job-level script/before_script/after_script
'(.. | select(has("script")) | .script) |= ("echo script in " + (parent | key) + " running at $(date)")',
'(.. | select(has("before_script")) | .before_script) |= ("echo before_script in " + (parent | key) + " running at $(date)")',
'(.. | select(has("after_script")) | .after_script) |= ("echo after_script in " + (parent | key) + " running at $(date)")',

# job-level image tags
'(.. | select(has("image")) | .image) |= ("alpine")',

# global before_script/after_script
'(. | select(has("before_script")) | .before_script) |= ("echo global before_script running at $(date)")',
'(. | select(has("after_script")) | .after_script) |= ("echo global after_script running at $(date)")',

# global image tag
'(. | select(has("image")) | .image) |= ("alpine")'
]


def process_ci_file(file_path, mode="clean", tags=None, extensions=None):
    """Process an individual CI configuration file"""

    # Check if the file has an allowed extension
    allowed_extensions = ["yml"]
    if extensions:
        allowed_extensions.extend([ext.lower() for ext in extensions.split(",")])

    file_extension = os.path.splitext(file_path)[1][1:]
    if file_extension.lower() not in allowed_extensions:
        print(f"Skipping file: {file_path} - invalid extension. Use --extensions to provide additional extensions to process.")
        return

    # If a custom Runner tag was supplied we replace existing tags, otherwise we remove all tags
    if tags:
        operations.append('(.. | select(has("tags")) | .tags) |= ("' + tags + ')')
    else:
        operations.append('(.. | select(has("tags")) | .tags) |= ("")')

    command = ['yq', '-i']

    try:
        if mode == 'normalize':
            subprocess.run(command + ['.'] + [file_path], check=False)
        else:
            for operation in operations:
                subprocess.run(command + [operation] + [file_path], check=False)
    except FileNotFoundError:
        print("The yq command is not available.")

def process_directory(directory_path, tags=None, extensions=None):
    """Process a directory containting multiple CI configuration files"""

    # Create clean and normalized copies of the directory
    clean_dir = directory_path + CLEAN_SUFFIX
    normalized_dir = directory_path + NORMALIZED_SUFFIX

    if os.path.exists(clean_dir):
        shutil.rmtree(clean_dir)
    shutil.copytree(directory_path, clean_dir)
    if os.path.exists(normalized_dir):
        shutil.rmtree(normalized_dir)
    shutil.copytree(directory_path, normalized_dir)

    # Process files in the clean directory
    process_files_in_directory(clean_dir, mode="clean", tags=tags, extensions=extensions)

    # Process files in the normalized directory
    process_files_in_directory(normalized_dir, mode="normalize", tags=tags, extensions=extensions)

def process_files_in_directory(directory, mode="clean", tags=None, extensions=None):
    """Process the individual files in a directory containing multiple CI configuration files"""

    for root, _, files in os.walk(directory):
        for file in files:
            file_path = os.path.join(root, file)
            process_ci_file(file_path, mode=mode, tags=tags, extensions=extensions)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Clean CI configuration files for easier reproduction.")
    parser.add_argument("path", help="File or directory path to process.", type=str)
    parser.add_argument("--tags", help="Replace `tags:` with this value instead of removing.", type=str, required=False)
    parser.add_argument("--extensions", help="Comma-separated list of file extensions to process beside `yml`.", type=str, required=False)
    args = parser.parse_args()

    arg_path = args.path
    arg_tags = args.tags
    arg_extensions = args.extensions

    if os.path.isfile(arg_path):
        base_name, extension = os.path.splitext(arg_path)
        shutil.copyfile(arg_path, f"{base_name}{NORMALIZED_SUFFIX}{extension}")
        shutil.copyfile(arg_path, f"{base_name}{CLEAN_SUFFIX}{extension}")
        process_ci_file(f"{base_name}{NORMALIZED_SUFFIX}{extension}", mode="normalize", tags=arg_tags, extensions=arg_extensions)
        process_ci_file(f"{base_name}{CLEAN_SUFFIX}{extension}", mode="clean", tags=arg_tags, extensions=arg_extensions)
    elif os.path.isdir(arg_path):
        process_directory(arg_path, tags=arg_tags, extensions=arg_extensions)
    else:
        print(f"Invalid input: {arg_path} is neither a file nor a directory.")
