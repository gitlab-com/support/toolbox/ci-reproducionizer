# CI Reproducionizer

Modifies CI configuration file(s) in a way that allows execution without context,
for example internal registries used with `image:`, custom Runner `tags:` and most
importantly, the entire `script:`, `before_script:` and `after_script:` parts.

This is useful for debugging issues unrelated to those aspects, for example complex
`:rules` configurations, `environment:` deployments and more.

## Requirements

Needs `yq` available in your PATH: https://github.com/mikefarah/yq/

## Usage

The tool can either handle a single file, or recursively modify an entire directory.

By default it only modifies `yml` files. Optionally, provide additional extenstions (for example `tpl`) via `--extensions`.

The input file(s) will **not** be modified. Two new files/directories will be created:
 - One with suffix `-normalized`, which serves as a baseline. `yq` can't preserve _all_ comments and whitespace fully, so this directory has the output after running the input through `yq` without actually changing anything.
 - One with suffix `-clean`, this is where the actual modifications happen. To see what was changed, it will be easier to diff this with `-normalized` than the actual input, so whitespace/comment changes don't make things harder to see.

```shell
$> ./ci_repro.py -h
usage: ci_repro.py [-h] [--tags TAGS] [--extensions EXTENSIONS] path

Clean CI configuration files for easier reproduction.

positional arguments:
  path                  File or directory path to process.

options:
  -h, --help            show this help message and exit
  --tags TAGS           Replace `tags:` with this value instead of removing.
  --extensions EXTENSIONS
                        Comma-separated list of file extensions to process beside `yml`.
```

## Contributing

Feel free to open an issue or merge request.
